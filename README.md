# Idolreq
Simple app dùng để nhận diện diễn diên AV.

# Getting started
1. Cài python3, pip, virtualenv
2. Sau khi cài đặt xong virtualenv chạy lệnh:
  ```
  # Tạo môi trường ảo
  $ virtualenv -p python3 venv

  # Activate môi trường ảo lên
  $ source venv/bin/activate

  # Cài đặt các packages cho môi trường
  $ pip install -r requirements.txt
  ```

3. Sau khi cài đặt xong các bước trên chạy lệnh để khởi động app:
```
$ python index.py
```

4. Test bằng cách: 

Gửi POST request tới URL: `/api/recognition` 
Params: 
  - `image`: File

Example:
```
curl -X POST -F image=@girl.png 'http://localhost:5000/api/'
```

# Architecture
![Main flow](docs/images/workflow.png "Main flow")

Train:
- Preprocessing: Sử dụng MTCNN (xem ở `idolreq/recognition/align/align_dataset_mtcnn.py`)
- Siamese matrix: Sử dụng Facenet (xem ở `idolreq/recognition/align/facenet.py`)
- Face classification: Sử dụng LinearSVC (xem ở `idolreq/recognition/training.py`)

Run:
- Main app được implement ở trong `idolreq/recognition/controller.py`

# Training data creation: 

Vào `idolreq/recognition` sau đó tạo 1 directory tên là `training_data` bỏ data vào đây như sau: 
```
../
training_data/
    1. Maria Ozawa/
        test1.png
        test2.png
        test3.png
    2. Yui Hatano/
        abc.png
        def.png
```

Chú ý tên directory phải theo dạng như sau: <number. name>


Và chạy:
```
python training.py --align=True
```
Để tạo ra `aligned_data` directory, đây là nơi chứa những khuôn mặt đã được cắt ra của các diễn viên.
Sau đó chạy: 

```
python training.py --train=True
```
Để đưa aligned data vào Siamese matrix và tạo ra 1 matrix (m, 512) làm input cho LinearSVC. 
Matrix này sẽ được lưu ở `database/data.h5`


# Author
- tuyenhx
