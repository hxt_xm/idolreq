import numpy as np
from scipy.spatial.distance import cdist


def test_compare():
    vec = np.random.randn(1, 512)
    maxtrix = np.random.randn(56, 512)
    result = np.squeeze(cdist(vec, maxtrix))
    print(result)
    index = np.argmin(result)
    print(result[index])
