# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
from flask import Flask

from idolreq.config import ProdConfig
from idolreq import recognition


def create_app(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprint(app)
    register_errorhandlers(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""


def register_blueprint(app):
    """Register Flask blueprints."""
    app.register_blueprint(recognition.controller.blueprint)


def register_errorhandlers(app):
    """Register error handler."""
