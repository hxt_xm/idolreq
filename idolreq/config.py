# -*- coding: utf-8 -*-


class Config(object):
    """ General config """
    DEBUG = False
    CSRF_ENABLED = True


class DevConfig(Config):
    """ Development config """
    DEBUG = True


class ProdConfig(Config):
    """ Production config """
    DEBUG = False
    CSRF_ENABLED = False
