#!/bin/bash

cd training_data

COUNTER=0

for f in * ;
do
  if [ -d "$f" ]
  then
    let COUNTER=COUNTER+1 
    # mv "$f" "$COUNTER.$f"
    NEW_NAME=$(echo "$f" | sed "s/[0-9]*\.//g")
    mv "$f" "$NEW_NAME"
  fi
done
