import pandas as pd
import os
import re
import functools
import h5py
import numpy as np
import tensorflow as tf
import math
import argparse
import sys

from os import path
from os.path import isdir

# Models
from sklearn.svm import SVC, LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

from classification.model import create_model
from keras.utils import to_categorical

from sklearn.externals import joblib
from scipy.misc import imresize
from scipy import misc
from align import facenet


def load_model(sess):
    """
    Load pretrain weights into tensorflow session

    Arguments:
        sess -- tensorflow session
    """
    print('------------------------------------')
    print('Loading tensorflow model ....       ')

    model_exp = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             'weights')

    meta_file = 'model-20180402-114759.meta'
    ckpt_file = 'model-20180402-114759.ckpt-275'

    # Load metadata and restore weights
    saver = tf.train.import_meta_graph(os.path.join(model_exp, meta_file))
    saver.restore(sess, os.path.join(model_exp, ckpt_file))

    print('The model was loaded successfully!   ')
    print('-------------------------------------')


#############################################
# Utils methods
#############################################
def compose(*functions):
    def compose(f, g):
        return lambda x: f(g(x))
    return functools.reduce(compose, functions, lambda x: x)


def get_current_directory_path():
    return path.join(path.dirname(path.abspath(__file__)))


def is_image_file(file):
    predicate = (
        file.endswith('.png')
        or file.endswith('.jpg')
    )
    return predicate


def is_generated_file(file):
    pattern = re.compile('^face_generated\d+.png$')
    return pattern.match(file)


def is_training_file(file):
    return file.endswith('.png')


def is_dir_in_training(dir):
    abs_path = path.join(get_current_directory_path(), 'aligned_data', dir)
    return isdir(abs_path)


def is_data_dir(dir):
    pattern = re.compile('(^\d+)\.(.*?)')
    return pattern.match(dir)


def extract_key_and_idol_name(input):
    split_results = re.compile('(^\d+)\.(.*?)').split(input)
    return (int(split_results[1]), split_results[3])


#############################################
# Utils methods
#############################################
def get_training_image_paths():
    images_path = []
    lbls = []

    training_dir = path.join(get_current_directory_path(), 'aligned_data')
    dirs = list(filter(is_dir_in_training, os.listdir(training_dir)))

    for data_dir in dirs:
        if (is_data_dir(data_dir)):
            abs_path = path.join(training_dir, data_dir)
            files = list(filter(is_training_file, os.listdir(abs_path)))
            for file in files:
                file_path = path.join(abs_path, file)
                images_path.append(file_path)

                key_and_idol_name = extract_key_and_idol_name(data_dir)
                lbls.append(key_and_idol_name)

    return images_path, lbls


def store_matrix(embeddeds, lbls):

    FILE_NAME = 'database/data.h5'

    if not path.exists(FILE_NAME):
        hf = h5py.File(FILE_NAME, 'w')
        hf.create_dataset('faces', data=embeddeds, maxshape=(None, 512), chunks=True)
        hf.create_dataset('labels', data=lbls, maxshape=(None, ), chunks=True)
    else:
        hf = h5py.File(FILE_NAME, 'a')
        faces = hf.get('faces')
        labels = hf.get('labels')

        m, _ = faces.shape
        new_m, _ = embeddeds.shape

        faces.resize((m + new_m, 512))
        labels.resize((m + new_m,))
        faces[m:m + new_m] = embeddeds
        labels[m:m + new_m] = lbls

    hf.close()


def display_matrix_info():
    FILE_NAME = 'database/data.h5'

    print('---------------------------')
    print('--      Matrix info      --')
    print('---------------------------')

    if not path.exists(FILE_NAME):
        print('{0} is not found'.format(FILE_NAME))
    else:
        hf = h5py.File(FILE_NAME, 'r')
        print(hf.get('faces'))
        print(hf.get('labels'))
        hf.close()


def load_matrix():
    FILE_NAME = 'database/data.h5'

    if not path.exists(FILE_NAME):
        print('{0} is not found'.format(FILE_NAME))
        # Return empty result
        X = np.array([])
        Y = np.array([])
    else:
        hf = h5py.File(FILE_NAME, 'r')
        X = np.array(hf.get('faces'))
        Y = np.array(hf.get('labels'))
        print('Loaded shape: ')
        print('X: ', X.shape)
        print('Y: ', Y.shape)

        hf.close()

    return X, Y


def read_data():
    with h5py.File('database/data.h5', 'r') as hf:
        embeddeds = np.array(hf.get('faces'))
        lbls = np.array(hf.get('labels'))
    return embeddeds, lbls


def store_lbls(lbls_dict):
    df = pd.DataFrame.from_dict(lbls_dict, orient='index')
    df.to_csv('database/lbls.csv', header=False)


def read_lbls_dict():
    df = pd.Series.from_csv('database/lbls.csv', header=None)
    return df.to_dict()


def process_lbls(text_lbls):
    seen = set()
    lbls_distinct = [item for item in text_lbls if item not in seen and not seen.add(item)]
    lbls_dict = {key: idol_name for key, idol_name in lbls_distinct}
    numb_lbls = list(map(lambda row: row[0], text_lbls))
    return numb_lbls, lbls_dict


def shuffle_data(X, Y):
    m, _ = X.shape
    p = np.random.permutation(m)

    return X[p], Y[p]


def split_data(X, Y):
    m, _ = X.shape

    split_1 = int(m * 0.8)
    split_2 = int(m * 0.9)

    X_train = X[:split_1]
    Y_train = Y[:split_1]

    X_dev = X[split_1:split_2]
    Y_dev = Y[split_1:split_2]

    X_test = X[split_2:]
    Y_test = Y[split_2:]

    return (X_train, Y_train, X_dev, Y_dev, X_test, Y_test)


def train_idol_comparison(X, Y):
    print('Shuffling and split datasets')
    X_shuffled, Y_shuffled = shuffle_data(X, Y)
    X_train, Y_train, X_dev, Y_dev, X_test, Y_test = split_data(X_shuffled, to_categorical(Y_shuffled))

    print('X_train shape: ', X_train.shape)
    print('Y_train shape: ', Y_train.shape)
    print('X_dev shape: ', X_dev.shape)
    print('Y_dev shape: ', Y_dev.shape)
    print('X_test shape: ', X_test.shape)
    print('Y_test shape: ', Y_test.shape)

    print('Begin training Keras model ...')

    model = create_model((X.shape[1],), Y_train.shape[1])
    model.summary()

    model.fit(X_train, Y_train, epochs=30, batch_size=128)

    print('Evaluate dev set: ')
    print(model.evaluate(X_dev, Y_dev, batch_size=128))

    print('Evaluate test set: ')
    print(model.evaluate(X_test, Y_test, batch_size=128))

    # print('Begin training LinearSVC...')
    # model = LinearSVC(C=1.5)
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training LogisticRegression...')
    # model = LogisticRegression()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training SVC...')
    # model = SVC()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training KNeighborsClassifier...')
    # model = KNeighborsClassifier(n_neighbors=3)
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training GaussianNB...')
    # model = GaussianNB()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training Perceptron...')
    # model = Perceptron()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training SGDClassifier...')
    # model = SGDClassifier(max_iter=1000)
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training DecisionTreeClassifier...')
    # model = DecisionTreeClassifier()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # print('Begin training RandomForestClassifier...')
    # model = RandomForestClassifier()
    # model.fit(X_train, Y_train)
    # train_error = round((1 - model.score(X_train, Y_train)) * 100, 2)
    # test_error = round((1 - model.score(X_test, Y_test)) * 100, 2)
    # print('Train error: {0}. Test error: {1}'.format(train_error, test_error))

    # Save model to file
    # model_file_name = 'comparision_model.pkl'
    # file_path = path.join(get_current_directory_path(), 'weights', model_file_name)
    # joblib.dump(model, file_path)
    # print('Model saved to: ', file_path)


def load_images_and_lbls(image_paths, numb_lbls):
    loaded_images = []
    loaded_numb_lbls = []
    for i in range(0, len(image_paths)):
        img_path = image_paths[i]
        lbl = numb_lbls[i]
        img = misc.imread(img_path, mode='RGB')
        if img is not None:
            image_size = 160
            img = imresize(img, (image_size, image_size), interp='bilinear')
            prewhitened = facenet.prewhiten(img)
            loaded_images.append(prewhitened)
            loaded_numb_lbls.append(lbl)
    loaded_images = np.stack(loaded_images)
    loaded_numb_lbls = np.stack(loaded_numb_lbls)

    return loaded_images, loaded_numb_lbls


def evaluate(sess, images):
    """
    Create output (?, 512) vector from input images

    Arguments:
        sess -- tensorflow session
        images -- (?, 160, 160, 3) list of image in form of a matrix

    Returns:
        embeddings -- (?, 512) embeddings vector of input images
    """

    # Input size: (?, 160, 160, 3)
    images_placeholder = graph.get_tensor_by_name('input:0')

    # Output size: (?, 512)
    embeddings = graph.get_tensor_by_name('embeddings:0')

    phase_train_placeholder = graph.get_tensor_by_name('phase_train:0')

    feed_dict = {images_placeholder: images, phase_train_placeholder: False}
    return sess.run(embeddings, feed_dict=feed_dict)


def process_data(img_paths_batch, numb_lbls_batch):
    loaded_images, loaded_lbls = load_images_and_lbls(img_paths_batch,
                                                      numb_lbls_batch)
    print('Starting encoding images ...')
    embedded_images = evaluate(sess, loaded_images)
    print('Finished encoding! Embedded shape: ', embedded_images.shape)

    return embedded_images, loaded_lbls


def generate_embedding():
    img_paths, lbls = get_training_image_paths()
    numb_lbls, lbls_dict = process_lbls(lbls)

    # Store lbls_dict
    store_lbls(lbls_dict)

    print('Number of loaded img_paths: ', len(img_paths))
    print('Number of loaded labels: ', len(numb_lbls))

    total_size = len(img_paths)
    batch_size = 512
    total_loop = math.floor(total_size/batch_size)

    for i in range(total_loop):
        range_from = i * batch_size
        range_to = range_from + batch_size

        print('Process batch: {0}/{1}'.format(i + 1, total_loop + 1))
        img_paths_batch = img_paths[range_from:range_to]
        numb_lbls_batch = numb_lbls[range_from:range_to]

        embedded_images, loaded_lbls = process_data(img_paths_batch,
                                                    numb_lbls_batch)

        # TODO: Append training data to the model
        store_matrix(embedded_images, loaded_lbls)

        if i % 2 == 0:
            display_matrix_info()

    # Process last batch
    range_from = range_to
    range_to = total_size

    print('Process batch: {0}/{1}'.format(i + 1, total_loop + 1))
    img_paths_batch = img_paths[range_from:range_to]
    numb_lbls_batch = numb_lbls[range_from:range_to]

    embedded_images, loaded_lbls = process_data(img_paths_batch,
                                                numb_lbls_batch)

    # TODO: Append training data to the model
    store_matrix(embedded_images, loaded_lbls)

    # TODO: Random dataset and train it
    # train_idol_comparison(embedded_images, loaded_lbls)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--align', type=bool,
                        help='Run align faces in trainig_data directory',
                        default=False)
    parser.add_argument('--train', type=bool,
                        help='Run train classifer',
                        default=False)
    return parser.parse_args(argv)


def main(args):
    if args.align:
        global graph
        global sess

        graph = tf.Graph()
        sess = tf.Session(graph=graph)

        with graph.as_default():
            load_model(sess)
            generate_embedding()
        return

    if args.train:
        X, Y = load_matrix()
        train_idol_comparison(X, Y)
        return


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
