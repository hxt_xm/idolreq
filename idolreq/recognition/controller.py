# -*- coding: utf-8 -*-
from flask import Blueprint, jsonify, request
from scipy.spatial.distance import cdist
from scipy.misc import imresize
from scipy import misc
from os import path
from sklearn.externals import joblib
from .align import detect_face, facenet

import tensorflow as tf
import numpy as np
import pandas as pd
import cv2
import os
import h5py


blueprint = Blueprint('recognition', __name__)


#####################################
#  Load model as global variable
#####################################
def get_current_directory_path():
    return os.path.join(os.path.dirname(os.path.abspath(__file__)))


def load_lbls_dict():
    cwd = get_current_directory_path()
    lbls_file_path = path.join(cwd, 'database/lbls.csv')
    lbls_dict = pd.Series.from_csv(lbls_file_path, header=None).to_dict()
    return lbls_dict


def load_idol_comparison_model():
    model_file_name = 'comparision_model.pkl'
    file_path = path.join(get_current_directory_path(), 'weights', model_file_name)
    model = joblib.load(file_path)
    return model


def load_model(sess):
    """
    Load pretrain weights into tensorflow session

    Arguments:
        sess -- tensorflow session
    """
    print('------------------------------------')
    print('Loading tensorflow model ....       ')

    model_exp = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             'weights')

    meta_file = 'model-20180402-114759.meta'
    ckpt_file = 'model-20180402-114759.ckpt-275'

    # Load metadata and restore weights
    saver = tf.train.import_meta_graph(os.path.join(model_exp, meta_file))
    saver.restore(sess, os.path.join(model_exp, ckpt_file))

    print('The model was loaded successfully!   ')
    print('-------------------------------------')


global graph
global sess
global lbls_dict
global idol_comparation_model
global pnet
global rnet
global onet

idol_comparation_model = load_idol_comparison_model()
lbls_dict = load_lbls_dict()
graph = tf.Graph()
sess = tf.Session(graph=graph)

with graph.as_default():
    pnet, rnet, onet = detect_face.create_mtcnn(sess, None)
    load_model(sess)


#####################################
#  Model utils
#####################################
def evaluate(sess, images, graph=graph):
    """
    Create output (?, 512) vector from input images

    Arguments:
        sess -- tensorflow session
        images -- (?, 160, 160, 3) list of image in form of a matrix

    Returns:
        embeddings -- (?, 512) embeddings vector of input images
    """

    # Input size: (?, 160, 160, 3)
    images_placeholder = graph.get_tensor_by_name('input:0')

    # Output size: (?, 512)
    embeddings = graph.get_tensor_by_name('embeddings:0')

    phase_train_placeholder = graph.get_tensor_by_name('phase_train:0')

    feed_dict = {images_placeholder: images, phase_train_placeholder: False}
    return sess.run(embeddings, feed_dict=feed_dict)


def get_embedded_faces(sess, faces):
    """
    Convert faces to embedding vectors

    Arguments:
        sess  -- tensorflow session
        faces  -- list of images size (n, 160, 160, 3)

    Returns:
        embedded vectors - size (n, 512)
    """
    return evaluate(sess, faces)


def compare_embedded_faces_to_db(embedded_faces):
    response = []

    for emb_face in embedded_faces:
        processed_emb_face = np.reshape(emb_face, (1, 512))

        idol_class = idol_comparation_model.predict(processed_emb_face)
        # probs = idol_comparation_model.predict_proba(processed_emb_face)

        index = idol_class[0]
        # probs = np.squeeze(probs)
        # prob = probs[np.argmax(probs)]
        prob = 0.0
        name = lbls_dict[index]

        response.append(
            {
                'name': name,
                'prob': prob
            }
        )

    return response


# def img_to_encoding(sess, image_path):
#     img_path = os.path.join(get_current_directory_path(), image_path)
#     print(img_path)
#     img = cv2.imread(img_path, 1)
#     img = cv2.resize(img, (160, 160), interpolation=cv2.INTER_CUBIC)
#     img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#     img = img.astype(np.float32)
#     embedding = evaluate(sess, np.expand_dims(img, axis=0))
#     return embedding


def imgs_to_encoding(sess, image_paths):

    loaded_images = []
    for img_path in image_paths:
        img = cv2.imread(img_path, 1)
        if img is not None:
            img = imresize(img, (160, 160, 3))
            img = img.astype(np.float32)
            loaded_images.append(img)
    loaded_images = np.array(loaded_images)
    embedded_images = evaluate(sess, loaded_images)
    return embedded_images


def distance(embedding_1, embedding_2):
    """
    Compute distance between 2 embeddings

    Arguments:
        embedding_1 {np.ndarray} -- Size: (, 512)
        embedding_2 {np.ndarray} -- Size: (, 512)

    Returns:
        distance -- distance between embedding_1 and embedding_2,
                    value range [0, 1]
    """
    return cdist(embedding_1, embedding_2, metric='euclidean')


def prepare_image(image_str):
    image = np.fromstring(image_str, np.uint8)
    return cv2.imdecode(image, cv2.IMREAD_COLOR)


def align_image_and_detect_boxes(image):

    results = []

    # Algorithm parameters
    minsize = 20                 # minimum size of face
    threshold = [0.6, 0.7, 0.7]  # three steps's threshold
    factor = 0.709               # scale factor

    bounding_boxes, _ = detect_face.detect_face(image, minsize, pnet, rnet, onet, threshold, factor)
    nrof_faces = bounding_boxes.shape[0]

    if nrof_faces > 0:
        det = bounding_boxes[:, 0:4]
        det_arr = []
        img_size = np.asarray(image.shape)[0:2]
        if nrof_faces > 1:
            for i in range(nrof_faces):
                det_arr.append(np.squeeze(det[i]))
        else:
            det_arr.append(np.squeeze(det))

        for i, det in enumerate(det_arr):
            det = np.squeeze(det)
            bb = np.zeros(4, dtype=np.int32)
            margin = 44
            bb[0] = np.maximum(det[0] - margin/2, 0)
            bb[1] = np.maximum(det[1] - margin/2, 0)
            bb[2] = np.minimum(det[2] + margin/2, img_size[1])
            bb[3] = np.minimum(det[3] + margin/2, img_size[0])
            cropped = image[bb[1]:bb[3], bb[0]:bb[2], :]
            image_size = 160
            scaled_face = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
            prewhitened = facenet.prewhiten(scaled_face)
            position_dict = {
                'x': int(bb[0]),
                'y': int(bb[1]),
                'w': int(bb[2] - bb[0]),
                'h': int(bb[3] - bb[1])
            }

            results.append((prewhitened, position_dict))

    return results


def detect_faces(image):
    """
    Detect faces from uploaded image

    Arguments:
        image {numpy.array} -- uploaded image

    Returns:
        list of position and face labels
    """

    # Store response data
    response = []

    aligned_data = align_image_and_detect_boxes(image)

    # Return if there is no detected face
    if len(aligned_data) == 0:
        return []

    face_positions = []
    faces = []
    for face, position in aligned_data:
        face_positions.append(position)
        faces.append(face)

    embedded_faces = get_embedded_faces(sess, np.array(faces))

    # Compare embedded faces with database
    lbl_props = compare_embedded_faces_to_db(embedded_faces)

    # Merge face_position with lbl_props to get reponse
    for i in range(0, len(face_positions)):
        position = face_positions[i]
        lbl_prop = lbl_props[i]

        response.append(
            {**position, **lbl_prop}
        )

    return response


#####################################
#  Route apis
#####################################


@blueprint.route('/api/recognition', methods=['POST'])
def analyse():

    response = {
        'data': []
    }

    if 'image' in request.files:
        image = prepare_image(request.files['image'].read())
        response['data'] += detect_faces(image)

    return jsonify(response)
