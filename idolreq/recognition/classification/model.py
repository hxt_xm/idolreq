from keras.layers import Input
from keras.layers.core import Dense, Activation, Dropout
from keras.models import Model
from keras.layers.normalization import BatchNormalization


def create_model(input_shape, num_of_classes):
    X_input = Input(input_shape, name='input_layer')

    X = Dense(1024, name='first_layer')(X_input)

    X = Dense(1024, name='second_layer')(X)
    X = Activation('relu')(X)
    X = BatchNormalization(axis=1, epsilon=0.00001, name='second_layer_batch_norm')(X)
    X = Dropout(0.8)(X)

    X = Dense(1024, name='third_layer', activation='relu')(X)
    X = Activation('relu')(X)
    X = BatchNormalization(axis=1, epsilon=0.00001, name='third_layer_batch_norm')(X)
    X = Dropout(0.8)(X)

    X = Dense(1024, name='fourth_layer', activation='relu')(X)
    X = Activation('relu')(X)
    X = BatchNormalization(axis=1, epsilon=0.00001, name='fourth_layer_batch_norm')(X)

    X = Dense(num_of_classes, name='output_layer', activation='softmax')(X)

    model = Model(inputs=X_input, outputs=X)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model


if __name__ == '__main__':
    model = create_model((1, 512), 10)
    model.summary()
