#!/bin/bash

cd training_data

COUNTER=0

for f in * ;
do
  if [ -d "$f" ]
  then
    let COUNTER=COUNTER+1 
    mv "$f" "$COUNTER.$f"
  fi
done
