# -*- coding: utf-8 -*-
import os
from idolreq.app import create_app
from idolreq.config import DevConfig, ProdConfig
from flask_cors import CORS


PYTHON_ENV = os.getenv('ENV')
app = create_app(DevConfig) if PYTHON_ENV == 'dev' else create_app(ProdConfig)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

if __name__ == '__main__':
    app.run(debug=True)
